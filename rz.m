function Rz = rz(A)
%%%
%
% rotation matrix (Z axis)
%
%%%

    Rz = [cos(A) -sin(A) 0;
	  sin(A) cos(A) 0;
	  0 0 1];

%%%EOF
