function MODEL = updateTag(MODEL)
%%%
%
% Update tags
%
%%%

    n     = MODEL.n;           % number of joints
    nTags = length(MODEL.Tag); % number of tags

    BasePosition = MODEL.Frame(1).T(1:3,4);

    %% update R, v, w, dv, dw
    for iT = 1:nTags % iterate over all tags
	tag = MODEL.Tag(iT);
	F   = MODEL.Frame(tag.P);

	%% -------------------------------------------------------
	%% update posture
	%% -------------------------------------------------------
	tag.T = F.T*tag.T_LOCAL;

	%% -------------------------------------------------------
	%% update velocity and acceleration
	%% -------------------------------------------------------
	r      = tag.T(1:3,4) - F.T(1:3,4);
	tag.w  = F.w;
	tag.v  = F.v + tilde(F.w)*r;
	tag.dw = F.dw;
	tag.dv = F.dv + tilde(F.dw)*r + tilde(F.w)*(tag.v-F.v);

	%% -------------------------------------------------------
	%% form the Jacobian matrices Av, Bv
	%% -------------------------------------------------------
	tag.Av = zeros(3,n);
	for k=1:length(F.joints)
	    JointIndex = F.joints(k);
	    FrameIndex = JointIndex + 1;

	    JointPosition = MODEL.Frame(FrameIndex).T(1:3,4);
	    JointAxis    = MODEL.Frame(FrameIndex).ax;

	    r = tag.T(1:3,4) - JointPosition;
	    tag.Av(:,JointIndex) = tilde(JointAxis)*r;
	end
	tag.Bv(:,4:6) = -tilde(tag.T(1:3,4) - BasePosition)*MODEL.E;
	%% -------------------------------------------------------

	MODEL.Tag(iT) = tag; % save
    end

%%%EOF
