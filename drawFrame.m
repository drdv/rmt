function drawFrame(T, scale)
%%%
%
% Plots a frame defined by a homogeneous matrix T (using a scale factor 'scale')
%
%%%

    if nargin < 2
	scale = 0.2;
    end

    R = T(1:3,1:3);
    r = T(1:3,4);

    color = {'b','g','r'};
    for i = 1:3
	U(:,1) = r;
	U(:,2) = r + s*R(:,i);
	plot3(U(1,:),U(2,:),U(3,:),'Color',color{i},'LineWidth',2);
    end

%%%EOF
