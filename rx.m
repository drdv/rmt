function Rx = rx(A)
%%%
%
% rotation matrix (X axis)
%
%%%

    Rx = [1 0 0;
	  0 cos(A) -sin(A);
	  0 sin(A) cos(A)];

%%%EOF
