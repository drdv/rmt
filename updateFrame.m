function MODEL = updateFrame(MODEL)
%%%
%
% Update frames
%
% NOTE: the only reason for including ddx as well is to be able to form
%       the inertia matrix by using only getInertialForces.m,
%       i.e., this is only for testing purposes.
%
%%%

    n   = MODEL.n; % number of joints
    x   = MODEL.x;
    dx  = MODEL.dx;
    ddx = MODEL.ddx; % see the NOTE above

    %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    %% update POSTURE
    %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Tq = eye(4);
    for iJ = 1:n  % iterate over all joints
	iL = iJ+1;  % iJ is the "input" joint for link iL
	F  = MODEL.Frame(iL);

	%% Rotation around axis at an angle (Rodrigues rotation formula)
	Tq(1:3,1:3) = eye(3) + tilde(F.ax_LOCAL)*sin(x(iJ)) + tilde(F.ax_LOCAL)^2*(1-cos(x(iJ)));

	F.T  = MODEL.Frame(F.P).T*F.T_LOCAL*Tq;
	F.ax = F.T(1:3,1:3)*F.ax_LOCAL;

	MODEL.Frame(iL) = F; % save
    end

    %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    %% update VELOCITY & ACCELERATION
    %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    %% --------------------------
    %% handle the base first
    %% --------------------------
    F    = MODEL.Frame(1);

    F.dv = zeros(3,1) + ddx(n+1:n+3);
    F.dw = MODEL.dE*dx(n+4:n+6) + MODEL.E*ddx(n+4:n+6);

    F.Aw = zeros(3,n);
    F.Bw(:,4:6) = MODEL.E;

    MODEL.Frame(1) = F; % save
    %% --------------------------

    for iJ = 1:n  % iterate over all joints
	iL = iJ+1;  % iJ is the "input" joint for link iL
	F  = MODEL.Frame(iL);
	iP = F.P;
	Fp = MODEL.Frame(iP);

	k  = F.ax; % joint axis of rotation expressed in Frame{WORLD}
	r  = F.T(1:3,4) - Fp.T(1:3,4); % Frame{Fp} to Frame{F} in Frame{WORLD}

	F.w = Fp.w + k*dx(iJ);
	F.v = Fp.v + tilde(Fp.w)*r;

	F.dw = Fp.dw + tilde(Fp.w)*k*dx(iJ) + k*ddx(iJ);
	F.dv = Fp.dv + tilde(Fp.dw)*r + tilde(Fp.w)*(F.v-Fp.v);

	%% --------------------------------------------------------------------
	%% find the joints supporting link iL and form Jacobian matrices
	%% --------------------------------------------------------------------
	S = iJ;
	while iP ~= 1 % while iP is not the base
	    S  = [S, iP-1];
	    iP = MODEL.Frame(iP).P;
	end
	S = sort(S); % sort in ascending order (for convenience)
	F.joints = S;
	%% --------------------------------------------------------------------
	F.Aw = zeros(3,n);
	for k=1:length(S)
	    JointIndex = S(k);
	    FrameIndex = JointIndex+1;
	    F.Aw(:,JointIndex) = MODEL.Frame(FrameIndex).ax;
	end
	F.Bw(:,4:6) = MODEL.E;
	%% --------------------------------------------------------------------

	MODEL.Frame(iL) = F; % save
    end

%%%EOF
