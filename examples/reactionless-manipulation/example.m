%%%A
%
% Planar manipulator with n links. At each sampling time a QP/hierarchy is solved
% and the state is propagated.
%
% Two options are implemented (with only equality constraints):
%
% --------------------------
% (I) hierarchy with objectives:
% --------------------------
% 1. impose dynamics (joint torques are eliminated)
% 2. zero base orientation
% 3. end-effector position
% 4. stop manipulator and base motion
% --------------------------
%
% --------------------------
% (II) QP with
% --------------------------
% 1. constraints
%    - impose dynamics (joint torques are eliminated)
%    - zero base orientation
%    - end-effector position
% 2. objective
%    - stop manipulator and base motion
% --------------------------
%
% Note: the lexls solver is not included.
%
%%%

addpath('~/local/opt/rmt')
addpath('~/local/opt/lexls/interfaces/matlab-octave')

clear;clc

%% ===================================================================

flag_display = 0;
flag_lex     = 0;

dt       = 0.005; % sampling time
max_iter = 1000;

%% ===================================================================

n  = 4;
s  = planarModel(n);
x  = zeros(n+6,1);
dx = zeros(n+6,1);

[JOINTS, BASE] = get_labels(n,6);
TAG_HAND       = get_labels(1);   % end-effector

%% ===================================================================

x(JOINTS) = linspace(0.1,0.9,n);

s = setState(s,x,dx);
offset = [-1.5;-0.5;0.0];
p_des  = s.Tag(TAG_HAND).T(1:3,4) + offset; % desired end-effector position

for iter = 1:max_iter

    fprintf('iter = %d\n', iter);

    s = setState(s,x,dx);
    [H,h] = getInertia(s);

    obj_ind = 0;

    %% -----------------------------
    obj_ind = obj_ind+1; % dynamics (joint torques are eliminated)
    %% -----------------------------
    obj(obj_ind).A  = H(BASE,:);
    obj(obj_ind).lb = -h(BASE);
    obj(obj_ind).ub = -h(BASE);
    %% -----------------------------

    %% -----------------------------
    obj_ind = obj_ind+1;
    Kp = 10;
    Kd = 5;
    %% -----------------------------
    A = zeros(1,n+6); A(BASE(end)) = 1;

    obj(obj_ind).A  = A;
    obj(obj_ind).lb = -Kp*x(BASE(end)) - Kd*dx(BASE(end));
    obj(obj_ind).ub = obj(obj_ind).lb;
    %% -----------------------------

    %% -----------------------------
    obj_ind = obj_ind+1; % TAG_HAND
    Kp = 10;
    Kd = 5;
    %% -----------------------------
    [J, dJdx] = getJacobian(s,TAG_HAND);
    J = J(1:3,:); dJdx = dJdx(1:3,:);
    p = s.Tag(TAG_HAND).T(1:3,4);

    %%J*ddx + dJdx = ddp '=' Kp*(p_des - p) + Kv*(dp_des - dp);
    %%                    =  Kp*(p_des - p) - Kv*J*dx
    obj(obj_ind).A  = J;
    obj(obj_ind).lb = Kp*(p_des - p) - Kd*J*dx - dJdx;
    obj(obj_ind).ub = obj(obj_ind).lb;
    %% -----------------------------

    %% -----------------------------
    obj_ind = obj_ind+1;
    Kp = 10;
    Kd = 5;
    %% -----------------------------
    obj(obj_ind).A  = eye(n+6);
    obj(obj_ind).lb = -Kp*x - Kd*dx;
    obj(obj_ind).ub = obj(obj_ind).lb;
    %% -----------------------------

    if flag_lex %% hierarchical problem
	ddx = lexlsi(obj);
    else        %% QP
	%% form constraints
	Aeq = []; beq = [];
	for i=1:3
	    Aeq = [Aeq;obj(i).A];
	    beq = [beq;obj(i).lb];
	end

	%% form objective: 0.5*(A*x - b)'*(A*x - b) => 0.5*x'*A'*A*x - x'*A'*b
	Q =  obj(4).A'*obj(4).A;
	q = -obj(4).A'*obj(4).lb;

	options = optimset('Display','off','Algorithm', 'active-set');
	ddx = quadprog(Q,q,[],[],Aeq,beq,[],[],[],options);
    end

    %% simple integration
    dx = dx + dt*ddx;
    x = x + dt*dx;

    if flag_display
	cla; drawModel(s);
	plot(p_des(1), p_des(2), 'gs')
	axis([-1 4 -1 4]); axis equal; grid on
	drawnow
    end

    %% output stuff
    X(:,iter)  = x;
    dX(:,iter) = dx;
    T(iter)    = iter*dt;
    P(:,iter)  = s.Tag(TAG_HAND).T(1:3,4);
    P_des(:,iter) = p_des;
end

close all

%% ----------------------------
figure
%% ----------------------------
subplot(3,1,1); hold on;
plot(T, P(1,:), 'b');
plot(T, P_des(1,:), 'r--');
grid on
title('tag position x')

subplot(3,1,2); hold on;
plot(T, P(2,:), 'b');
plot(T, P_des(2,:), 'r--');
grid on
title('tag position y')

subplot(3,1,3); hold on;
plot(T, X(BASE(end),:), 'b');
grid on
title('base orientation z')

%% ----------------------------
figure
%% ----------------------------
for i=1:n
    subplot(n,1,i); hold on;
    plot(T, X(i,:), 'b');
    grid on
    title(['joint angle',i])
end

%%%EOF
