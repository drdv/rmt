addpath('~/local/opt/bmsd/dynamics')
addpath('~/local/opt/bmsd/general_purpose')
addpath('~/local/opt/bmsd/kinematics')
addpath('~/local/opt/bmsd/rotation')
addpath('~/local/opt/bmsd/rotation')
addpath('~/local/opt/bmsd/models')

clear;clc

%% =====================================================

%% define models
SP  = model_test();
SV  = System_Variables(SP);
rmt = bmsd2rmt(SP);
n   = rmt.n;

ARM         = 1:n;
TRANSLATION = n+1:n+3;
ROTATION    = n+4:n+6;

%% set state for rmt model
x         = randn(n+6,1);
dx        = randn(n+6,1);
rmt       = setState(rmt, x, dx);

%% set state for bmsd model
SV.q      = x(ARM);
SV.L(1).p = x(TRANSLATION);
SV.L(1).R = e2R(x(ROTATION));
SV.dq     = dx(ARM);
SV.L(1).v = dx(TRANSLATION);
SV.L(1).w = dx(ROTATION);
SV        = calc_pos(SP,SV);

Gravity = [0;0;-9.81];

%% =====================================================

[H,h] = getInertia(rmt, Gravity);

H1 = calc_hh(SP,SV);
h1 = r_ne(SP,SV,Gravity);

[p_ee, R_ee] = fk_e(SP,SV,SP.bN,SP.bP);
p            = fk_j(SP,SV,1:n);

% =====================================================

for i=1:n+1
    e1(i) = norm(rmt.DP(i).c - SV.L(i).p);
end

for i=1:n
    e2(i) = norm(rmt.Frame(1+i).T(1:3,4) - p(:,i));
end

e3 = rmt.Tag.T - [R_ee, p_ee; 0 0 0 1];
e4 = H - permute_H(H1);
e5 = h - [h1(7:end);h1(1:6)];

fprintf('error position of all CoMs     = %e\n', norm(e1));
fprintf('error position of all joints   = %e\n', norm(e2));
fprintf('error position of end-effector = %e\n', norm(e3));
fprintf('error H                        = %e\n', norm(e4));
fprintf('error h                        = %e\n', norm(e5));

% =====================================================

if 0
    opt = defineDrawOptions('draw_com',1, 'color_com','m');
    hold on
    drawModel(rmt, opt);
    Draw_System(SP, SV, SP.bN, SP.bP,[]);
    axis equal; axis tight; grid on
end

%%%EOF
