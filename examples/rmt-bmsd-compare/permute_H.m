function Hp = permute_H(H)
%%%
%
% bMSd --> rmt inertia matrix
%
% bMSd: [Hb   Hbm;
%        Hbm'  Hm]
% rmt:  [Hm  Hbm';
%        Hbm  Hb]
%
%%%

    n  = size(H,1);
    Hp = zeros(n,n);

    Hp(1:n-6,1:n-6) = H(7:n,7:n);
    Hp(n-5:n,n-5:n) = H(1:6,1:6);
    Hp(n-5:n,1:n-6) = H(1:6,7:n);
    Hp(1:n-6,n-5:n) = H(7:n,1:6);

%%%EOF
