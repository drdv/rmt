function opt = defineDrawOptions(varargin)
%%%
%
% Define options used in drawModel.m
%
%%%

    show_options_flag = 0;

    %% ------------------------
    %% default options
    %% ------------------------
    opt.draw_com     = false;
    opt.draw_tag_num = false;
    opt.draw_frame   = false;

    opt.size_joint   = 8;
    opt.size_tag     = 12;
    opt.size_com     = 12;
    opt.size_tag_num = 18;

    opt.color_link   = 'k';
    opt.color_joints = 'r';
    opt.color_com    = 'g';
    opt.color_tag    = 'b';

    opt.line_width   = 1;
    %% ------------------------

    for i=1:2:length(varargin)
	name = varargin{i};
	val  = varargin{i+1};
	switch name
	    case 'draw_com'
		opt.draw_com     = val;
	    case 'draw_tag_num'
		opt.draw_tag_num = val;
	    case 'draw_frame'
		opt.draw_frame   = val;
	    case 'size_joint'
		opt.size_joint   = val;
	    case 'size_tag'
		opt.size_tag     = val;
	    case 'size_com'
		opt.size_com     = val;
	    case 'size_tag_num'
		opt.size_tag_num = val;
	    case 'color_link'
		opt.color_link   = val;
	    case 'color_joints'
		opt.color_joints = val;
	    case 'color_com'
		opt.color_com    = val;
	    case 'color_tag'
		opt.color_tag    = val;
	    case 'line_width'
		opt.line_width   = val;
	    otherwise
		fprintf('WARNING: unknown oprint flag (defineDrawOptions.m)\n');
	end
    end

    if show_options_flag
	fprintf('draw_com     = %d\n', opt.draw_com);
	fprintf('draw_tag_num = %d\n', opt.draw_tag_num);
	fprintf('draw_frame   = %d\n', opt.draw_frame);

	fprintf('size_joint   = %d\n', opt.size_joint);
	fprintf('size_tag     = %d\n', opt.size_tag);
	fprintf('size_com     = %d\n', opt.size_com);
	fprintf('size_tag_num = %d\n', opt.size_tag_num);

	fprintf('color_link   = %s\n', opt.color_link);
	fprintf('color_joints = %s\n', opt.color_joints);
	fprintf('color_com    = %s\n', opt.color_com);
	fprintf('color_tag    = %s\n', opt.color_tag);

	fprintf('line_width   = %d\n', opt.line_width);

	%% to do it automatically
	%%names = fieldnames(opt);
	%%for i=1:length(names)
	%%    fprintf('%s = %d\n', names{i}, getfield(opt, names{i}));
	%%end
    end

%%%EOF
