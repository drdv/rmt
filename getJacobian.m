function [J,dJdx] = getJacobian(MODEL,tag_number)
%%%
%
% Forms the Jacobian and nonlinear term for a given tag
%
%%%

    tag = MODEL.Tag(tag_number);
    F   = MODEL.Frame(tag.P);

    J = [tag.Av, tag.Bv;
	 F.Aw,   F.Bw];

    dJdx = [tag.dv;
	    tag.dw];

%%%EOF
