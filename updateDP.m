function MODEL = updateDP(MODEL)
%%%
%
% Update dynamic parameters
%
%%%

    n = MODEL.n; % number of joints

    BasePosition = MODEL.Frame(1).T(1:3,4);

    CoM    = zeros(3,1);
    mTotal = 0; % total mass of the robot
    %% update c, dc, ddc, I
    for iL = 1:n+1 % iterate over all links
	F = MODEL.Frame(iL);
	D = MODEL.DP(iL);

	%% ----------------------------------------------------
	%% position of CoM in Frame{WORLD}
	%% ----------------------------------------------------
	tmp = F.T*[D.c_LOCAL;1];
	D.c = tmp(1:3);

	%% ----------------------------------------------------
	%% velocity & acceleration of CoM in Frame{WORLD}
	%% ----------------------------------------------------
	r     = D.c - F.T(1:3,4);
	D.dc  = F.v + tilde(F.w)*r;
	D.ddc = F.dv + tilde(F.dw)*r + tilde(F.w)*(D.dc-F.v);

	%% ----------------------------------------------------
	%% Inertia matrix in Frame{WORLD}
	%% ----------------------------------------------------
	R   = F.T(1:3,1:3);
	D.I = R*D.I_LOCAL*R';

	%% -------------------------------------------------------
	%% form the Jacobian matrices Av, Bv
	%% -------------------------------------------------------
	D.Av = zeros(3,n);
	for k=1:length(F.joints)
	    JointIndex = F.joints(k);
	    FrameIndex = JointIndex + 1;

	    JoitPosition = MODEL.Frame(FrameIndex).T(1:3,4);
	    JointAxis    = MODEL.Frame(FrameIndex).ax;

	    r = D.c - JoitPosition;
	    D.Av(:,JointIndex) = tilde(JointAxis)*r;
	end
	D.Bv(:,4:6) = -tilde(D.c - BasePosition)*MODEL.E;
	%% -------------------------------------------------------

	MODEL.DP(iL) = D; % save

	mTotal = mTotal + D.m;
	CoM = CoM + D.m*D.c;
    end
    MODEL.CoM = CoM/mTotal;

%%%EOF
