function s = defineDP(c, m, I)
%%%
%
% Set the dynamic parameters of a link
%
% Input:
% ------
% c - center of mass expressed in Frame{LINK}
% m - mass of link
% I - inertia matrix of link expressed in Frame{LINK} and about the center of mass
%
% Output:
% -------
% s - structure containing the definition of the dynamic parameters
%
%%%

    %% ---------------------------------------------------
    %% Constant parameters
    %% ---------------------------------------------------

    s.c_LOCAL = c;
    s.I_LOCAL = I;
    s.m       = m;

    %% ---------------------------------------------------
    %% Variable parameters (expressed in Frame{WORLD})
    %% ---------------------------------------------------

    s.c   = zeros(3,1); % CoM in the world frame
    s.dc  = zeros(3,1); % linear velocity of the CoM in the world frame
    s.ddc = zeros(3,1); % linear acceleration of the CoM in the world frame

    s.I   = eye(3);     % inertia matrix expressed in the world frame (about the CoM of the link)

    s.Av  = [];         % Jacobian (linear) w.r.t. motion of joints
    s.Bv  = [eye(3), zeros(3,3)]; % Jacobian (linear) w.r.t. motion of base

%%%EOF
